package Playground

import generators.agent._
import org.apache.flink.api.common.eventtime.{SerializableTimestampAssigner, WatermarkStrategy}
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.scala._

object Exercise {

  private def processAgentReports(): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    val reportEvents = env.addSource(new AgentReportGenerator(500, 2))
      .assignTimestampsAndWatermarks( // extract timestamps for events (event time) + watermarks
        WatermarkStrategy
          .forBoundedOutOfOrderness(java.time.Duration.ofMillis(5000)) // once you get an event with time T, you will NOT accept further events with time < T - 5000
          .withTimestampAssigner(new SerializableTimestampAssigner[AgentEvent] {
            override def extractTimestamp(element: AgentEvent, recordTimestamp: Long): Long =
              element.eventTime.toEpochMilli
          })
      )

    reportEvents.print()

    env.execute()
  }

  def main(args: Array[String]): Unit = {
    processAgentReports()
  }
}
