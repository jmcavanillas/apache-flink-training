package Essentials

import org.apache.flink.api.common.functions._
import org.apache.flink.streaming.api.functions._
import org.apache.flink.streaming.api.scala._
import org.apache.flink.util.Collector

object ExplicitFunctions {

  private def explicitFunctions(): Unit = {
    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    val numbers = env.fromSequence(1, 100)

    // map
    val doubledNumbers = numbers.map(_ * 2)

    // explicit version
    val doubledNumbers_v2 = numbers.map(new MapFunction[Long, Long] {
      // declare fields, methods, ...
      override def map(value: Long): Long = value * 2
    })

    // flatMap
    val expandedNumbers = numbers.flatMap(n => Range.Long(1, n, 1).toList)

    // explicit version
    val expandedNumbers_v2 = numbers.flatMap(new FlatMapFunction[Long, Long] {
      // declare fields, methods, ...
      override def flatMap(n: Long, out: Collector[Long]): Unit =
        Range.Long(1, n, 1).foreach { i =>
          out.collect(i) // imperative style - pushes the new element downstream
        }
    })

    // process method
    // ProcessFunction is THE MOST GENERAL function to process elements in Flink
    val expandedNumbers_v3 = numbers.process(new ProcessFunction[Long, Long] {
      override def processElement(n: Long, ctx: ProcessFunction[Long, Long]#Context, out: Collector[Long]): Unit =
        Range.Long(1, n, 1).foreach { i =>
          out.collect(i)
        }
    })

    expandedNumbers_v3.print()
    env.execute()
  }

  private def sumByKey(): Unit = {
    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    val numbers = env.fromSequence(1, 100)

    // reduce
    // happens on keyed streams
    /*
      [ 1, false
        2, true

        100, true

      true => 2, 6, 12, 20, ...
      false => 1, 4, 9, 16, ...
     */
    val keyedNumbers: KeyedStream[Long, Boolean] = numbers.keyBy(n => n % 2 == 0)

    // reduce - FP approach
    val sumByKey = keyedNumbers.reduce(_ + _) // sum up all the elements BY KEY

    // reduce - explicit approach
    val sumByKey_v2 = keyedNumbers.reduce(new ReduceFunction[Long] {
      // additional fields, methods...
      override def reduce(x: Long, y: Long): Long = x + y
    })

    sumByKey_v2.print()
    env.execute()
  }

  def main(args: Array[String]): Unit = {
    explicitFunctions()
    // sumByKey()
  }
}
