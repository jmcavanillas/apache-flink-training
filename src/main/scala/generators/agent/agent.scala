package generators

import generators.agent.AgentInstalledAppsGenerator.getRandomInstalledApps
import generators.agent.AgentReportGenerator.{generateRandomAppMetrics, getRandomFileInfo, getRandomMetricReport, getRandomReport, getRandomUser, getRandomWorkSpace, workspaces}
import org.apache.flink.streaming.api.functions.source.SourceFunction

import java.time.Instant
import java.util.UUID
import scala.annotation.tailrec
import scala.util.Random

package object agent {

  sealed trait AgentEvent {
    def eventTime: Instant

    def getId: String
  }

  case class MetricReport(min: Double, max: Double, avg: Double)

  object MetricReport {
    private def roundTo2Decimals(value: Double): Double = {
      BigDecimal(value).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
    }

    def apply(min: Double, max: Double, avg: Double): MetricReport =
      new MetricReport(roundTo2Decimals(min), roundTo2Decimals(max), roundTo2Decimals(avg))
  }


  case class FileInfo(name: String, description: String, version: String)

  case class RunningProcess
  (
    fileInfo: FileInfo,
    cpu: MetricReport,
    ram: MetricReport,
    gpu: MetricReport,
  )

  final case class AgentReport
  (
    eventTime: Instant,
    id: UUID,
    workspaceId: String,
    user: String,
    cpu: MetricReport,
    ram: MetricReport,
    gpu: MetricReport,
    list: Seq[RunningProcess]
  ) extends AgentEvent {
    override def getId: String = s"report|$id"
  }

  final case class InstalledApps
  (
    eventTime: Instant,
    id: UUID,
    workspaceId: String,
    user: String,
    list: Seq[FileInfo]
  ) extends AgentEvent {
    override def getId: String = s"installed-apps|$id"
  }

  class AgentReportGenerator(
                              sleepMillisPerEvent: Int,
                              batchSize: Int,
                              baseInstant: java.time.Instant = java.time.Instant.now()
                            ) extends SourceFunction[AgentEvent] {


    @volatile private var running = true

    @tailrec
    private def run(
                     startId: Long,
                     ctx: SourceFunction.SourceContext[AgentEvent]
                   ): Unit =
      if (running) {
        generateRandomEvents(startId).foreach(ctx.collect)
        Thread.sleep(batchSize * sleepMillisPerEvent)
        run(startId + batchSize, ctx)
      }

    private def generateRandomEvents(id: Long): Seq[AgentEvent] = {
      val events = (1 to batchSize)
        .map(_ => getRandomReport(baseInstant.plusSeconds(id)))

      events
    }

    override def run(
                      ctx: SourceFunction.SourceContext[AgentEvent]
                    ): Unit = run(0, ctx)

    override def cancel(): Unit = {
      running = false
    }
  }

  object AgentReportGenerator {
    val users: Vector[String] = Vector("Sebas", "Kilian", "Josep", "Adria", "Alba", "Javier")
    val workspaces: Vector[String] = Vector("Workspace-1", "Workspace-3", "Workspace-56", "Workspace-2", "Workspace-14")
    val apps: Vector[FileInfo] = Vector(
      FileInfo("WhatsApp", "Messaging and calling app", "1.0.0"),
      FileInfo("Instagram", "Social media platform for sharing photos and videos", "1.0.0"),
      FileInfo("Facebook", "Social networking platform", "1.0.0"),
      FileInfo("Twitter", "Microblogging and social networking app", "1.0.0"),
      FileInfo("Google Maps", "Mapping and navigation app", "1.0.0"),
      FileInfo("YouTube", "Video sharing and streaming platform", "1.0.0"),
      FileInfo("Spotify", "Music streaming service", "1.0.0"),
      FileInfo("Netflix", "Video streaming platform for movies and TV shows", "1.0.0"),
      FileInfo("Microsoft Word", "Word processing software", "1.0.0"),
      FileInfo("Adobe Photoshop", "Graphic editing software", "1.0.0"),
      FileInfo("Zoom", "Video conferencing app", "1.0.0"),
      FileInfo("Slack", "Team communication and collaboration app", "1.0.0"),
      FileInfo("League of Legends", "Multiplayer online battle arena (MOBA) game", "1.0.0"),
      FileInfo("Baldur's Gate", "Role-playing video game", "1.0.0"),
      FileInfo("StarCraft II", "Real-time strategy game", "1.0.0"),
      FileInfo("Dawn of War", "Real-time strategy game set in the Warhammer 40,000 universe", "1.0.0"),
      // Adding Windows background services
      FileInfo("Windows Update", "Service for updating Windows operating system", "1.0.0"),
      FileInfo("Windows Defender", "Antivirus and security service", "1.0.0"),
      FileInfo("Windows Search", "Service for searching files and content on your PC", "1.0.0"),
      FileInfo("Windows Time", "Service for maintaining system time synchronization", "1.0.0"),
      FileInfo("Windows Audio", "Service for managing audio playback and recording", "1.0.0")
    )

    val versions: Vector[String] = Vector("1.0.1", "1.3", "2.3.0")

    def getRandomFileInfo: FileInfo = apps(Random.nextInt(apps.length)).copy(version = versions(Random.nextInt(versions.length)))

    def getRandomUser: String = s"${users(Random.nextInt(users.length))}"

    def getRandomWorkSpace: String = workspaces(Random.nextInt(workspaces.length))

    def getRandomMetricReport(min: Double, max: Double): MetricReport = {
      val minValue = min
      val maxValue = max

      if (minValue >= maxValue) {
        throw new IllegalArgumentException("minValue must be less than maxValue")
      }

      // Generate randomMin and randomMax ensuring randomMin < randomMax
      val randomMin = minValue + Random.nextDouble() * (maxValue - minValue)
      val randomMax = randomMin + Random.nextDouble() * (maxValue - randomMin)

      // Ensure that randomAvg is within the range of randomMin and randomMax
      val randomAvg = randomMin + Random.nextDouble() * (randomMax - randomMin)

      MetricReport(randomMin, randomMax, randomAvg)
    }

    def getRandomReport(at: Instant): AgentReport = {
      val cpu = getRandomMetricReport(0, 100)
      val ram = getRandomMetricReport(5, 16128)
      val gpu = getRandomMetricReport(0, 100)
      val workspace = getRandomWorkSpace

      AgentReport(
        at,
        UUID.randomUUID(),
        workspace,
        s"$workspace/$getRandomUser",
        cpu,
        ram,
        gpu,
        generateRandomAppMetrics((cpu, ram, gpu), Random.nextInt(10)).map {
          case (appCpu, appRam, appGpu) =>
            RunningProcess(getRandomFileInfo, appCpu, appRam, appGpu)
        }
      )
    }

    def generateRandomReportsFromBase(baseReport: MetricReport, n: Int): Vector[MetricReport] = {
      val random = new Random()

      val baseMin = baseReport.min
      val baseMax = baseReport.max
      val baseAvg = baseReport.avg

      val generatedReports = (1 to n).map(_ => {
        // Generate a random value within the range of baseMax and baseMin
        val randomValue = baseMin + random.nextDouble() * (baseMax - baseMin)

        // Calculate a corresponding randomAvg that maintains the same overall average
        val randomAvg = (n * baseAvg + randomValue) / (n + 1)

        MetricReport(baseMin, baseMax, randomAvg)
      }).toVector

      generatedReports
    }

    def generateRandomAppMetrics(baseReports: (MetricReport, MetricReport, MetricReport), n: Int)
    : Vector[(MetricReport, MetricReport, MetricReport)] = {
      val (baseReport1, baseReport2, baseReport3) = baseReports

      val randomReports1 = generateRandomReportsFromBase(baseReport1, n)
      val randomReports2 = generateRandomReportsFromBase(baseReport2, n)
      val randomReports3 = generateRandomReportsFromBase(baseReport3, n)

      randomReports1.zip(randomReports2).zip(randomReports3).map { case ((report1, report2), report3) =>
        (report1, report2, report3)
      }
    }
  }

  class AgentInstalledAppsGenerator(
                                     sleepMillisPerEvent: Int,
                                     batchSize: Int,
                                     baseInstant: java.time.Instant = java.time.Instant.now()
                                   ) extends SourceFunction[AgentEvent] {


    @volatile private var running = true

    @tailrec
    private def run(
                     startId: Long,
                     ctx: SourceFunction.SourceContext[AgentEvent]
                   ): Unit =
      if (running) {
        generateRandomEvents(startId).foreach(ctx.collect)
        Thread.sleep(batchSize * sleepMillisPerEvent)
        run(startId + batchSize, ctx)
      }

    private def generateRandomEvents(id: Long): Seq[AgentEvent] = {
      val events = (1 to batchSize)
        .map(_ => getRandomInstalledApps(baseInstant.plusSeconds(id)))

      events
    }

    override def run(
                      ctx: SourceFunction.SourceContext[AgentEvent]
                    ): Unit = run(0, ctx)

    override def cancel(): Unit = {
      running = false
    }
  }

  object AgentInstalledAppsGenerator {
    val users: Vector[String] = Vector("Sebas", "Kilian", "Josep", "Adria", "Alba", "Javier")
    val workspaces: Vector[String] = Vector("Workspace-1", "Workspace-3", "Workspace-56", "Workspace-2", "Workspace-14")
    val apps: Vector[FileInfo] = Vector(
      FileInfo("WhatsApp", "Messaging and calling app", "1.0.0"),
      FileInfo("Instagram", "Social media platform for sharing photos and videos", "1.0.0"),
      FileInfo("Facebook", "Social networking platform", "1.0.0"),
      FileInfo("Twitter", "Microblogging and social networking app", "1.0.0"),
      FileInfo("Google Maps", "Mapping and navigation app", "1.0.0"),
      FileInfo("YouTube", "Video sharing and streaming platform", "1.0.0"),
      FileInfo("Spotify", "Music streaming service", "1.0.0"),
      FileInfo("Netflix", "Video streaming platform for movies and TV shows", "1.0.0"),
      FileInfo("Microsoft Word", "Word processing software", "1.0.0"),
      FileInfo("Adobe Photoshop", "Graphic editing software", "1.0.0"),
      FileInfo("Zoom", "Video conferencing app", "1.0.0"),
      FileInfo("Slack", "Team communication and collaboration app", "1.0.0"),
      FileInfo("League of Legends", "Multiplayer online battle arena (MOBA) game", "1.0.0"),
      FileInfo("Baldur's Gate", "Role-playing video game", "1.0.0"),
      FileInfo("StarCraft II", "Real-time strategy game", "1.0.0"),
      FileInfo("Dawn of War", "Real-time strategy game set in the Warhammer 40,000 universe", "1.0.0"),
      // Adding Windows background services
      FileInfo("Windows Update", "Service for updating Windows operating system", "1.0.0"),
      FileInfo("Windows Defender", "Antivirus and security service", "1.0.0"),
      FileInfo("Windows Search", "Service for searching files and content on your PC", "1.0.0"),
      FileInfo("Windows Time", "Service for maintaining system time synchronization", "1.0.0"),
      FileInfo("Windows Audio", "Service for managing audio playback and recording", "1.0.0")
    )

    private val versions: Vector[String] = Vector("1.0.1", "1.3", "2.3.0")

    private def getRandomFileInfo: FileInfo = apps(Random.nextInt(apps.length)).copy(version = versions(Random.nextInt(versions.length)))

    private def getRandomUser: String = s"${users(Random.nextInt(users.length))}"

    private def getRandomWorkSpace: String = workspaces(Random.nextInt(workspaces.length))

    def getRandomInstalledApps(at: Instant): InstalledApps = {

      val workspace = getRandomWorkSpace

      InstalledApps(
        at,
        UUID.randomUUID(),
        workspace,
        s"$workspace/$getRandomUser",
        (1 to Random.nextInt(10)).map(_ => getRandomFileInfo)
      )
    }
  }

}
